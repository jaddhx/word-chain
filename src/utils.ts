export const getLastElement = <T>(array: T[]) => array.slice(-1)[0];

export const getFirstCharacter = (word: string) => word[0];

export const getLastCharacter = (word: string) => word.slice(-1)[0];

export const getRandomElement = <T>(array: T[]) =>
  array[Math.floor(Math.random() * array.length)];

export const groupByFirstLetter = (array: string[]) =>
  array.reduce<Record<string, string[]>>((acc, element) => {
    const group = element[0];
    if (!acc[group]) acc[group] = [element];
    else acc[group].push(element);
    return acc;
  }, {});
