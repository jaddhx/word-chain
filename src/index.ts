import {
  getFirstCharacter,
  getLastCharacter,
  getLastElement,
  getRandomElement,
  groupByFirstLetter,
} from "./utils";

export enum ResultType {
  FAIL,
  PASS,
  END,
}

export interface Result {
  resultType: ResultType;
  score: number;
}

export interface Pass extends Result {
  resultType: ResultType.PASS;
  word: string;
}

export interface Fail extends Result {
  resultType: ResultType.FAIL;
}

export interface End extends Result {
  resultType: ResultType.END;
}

export const isPass = (result: Result): result is Pass =>
  result.resultType === ResultType.PASS;

export default class WordChain {
  private readonly dictionary: Record<string, string[]>;
  private readonly previousWords: string[] = [];

  constructor(dictionary: string[]) {
    if (dictionary.length === 0) {
      throw new Error(
        "Cannot create a Word Chain game with an empty dictionary."
      );
    }
    this.dictionary = groupByFirstLetter(dictionary);
  }

  start(): Pass {
    if (this.previousWords.length > 0) {
      throw new Error("Cannot restart a game that has already been started.");
    }

    const word = getRandomElement(
      getRandomElement(Object.values(this.dictionary))
    );
    this.previousWords.push(word);
    return { resultType: ResultType.PASS, word, score: 0 };
  }

  play(word: string): Pass | Fail | End {
    const previousWord = getLastElement(this.previousWords);

    if (
      getFirstCharacter(word) !== getLastCharacter(previousWord) ||
      this.previousWords.includes(word) ||
      !this.isWordInDictionary(word)
    ) {
      return { resultType: ResultType.FAIL, score: this.getScore() };
    }

    this.previousWords.push(word);

    try {
      const nextWord = this.getNextWord(word);
      this.previousWords.push(nextWord);
      return {
        resultType: ResultType.PASS,
        word: nextWord,
        score: this.getScore(),
      };
    } catch {
      return { resultType: ResultType.END, score: this.getScore() };
    }
  }

  getScore() {
    return Math.floor(this.previousWords.length / 2);
  }

  private isWordInDictionary(word: string) {
    return Boolean(this.dictionary[getFirstCharacter(word)]?.includes(word));
  }

  private getNextWord(previousWord: string) {
    const eligibleNextWords =
      this.dictionary[getLastCharacter(previousWord)]?.filter(
        (word) => !this.previousWords.includes(word)
      ) || [];
    if (eligibleNextWords.length > 0) {
      return getRandomElement(eligibleNextWords);
    } else {
      throw new Error("Could not find a next word.");
    }
  }
}
